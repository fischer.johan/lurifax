const traverse = {

    counter : 0,

    getCachedSiteInfo: (cache, url) => {

        const siteInfo = { 'siteId': url }
        const [head, ...rest] = url.split('/');

        return traverse.recursive(cache[head], rest , siteInfo);
    },


    recursive: (cache, urlArray, result) => {

        if(!cache){
            return result;
        }

        const [head, ...rest] = urlArray;
        result = cache['type'] ? { ...result, 'type': cache['type'] } : result;

        if (!head || !cache.site || !cache.site[head]) {
            return result
        } else {
            return traverse.recursive(cache.site[head], rest, result)
        }
    },

    cacheSiteInfo: (cache, urlArray, siteInfo) => {
        if(!cache.contains(url2Domain(urlArray))){
            cache.insert(url2Domain(urlArray), {});
        }
        return traverse.recursiveCreate(cache.cacheObj, urlArray.split('/'), siteInfo);
    },


    recursiveCreate: (cache, urlArray, siteInfo) => {
        const [head, ...rest] = urlArray;

        if (!head) {
            traverse.copyAttributes(siteInfo, cache);
            return cache;
        }


        if (!cache.site || !cache.site[head]) {
            traverse.counter+=1;
            
            if(!cache.site){
                cache.site = {};
            }

            cache.site[head] = {}
        }


        return traverse.recursiveCreate(cache.site[head], rest,siteInfo)

    },

    copyAttributes: (from, to) => {
        for (var k in from) { to[k] = from[k] };
    }

}
