/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */

/*
  Utility functions needed by the front and backends of the extension
  */

function getTabId() {

}



function url2Domain(url) {

    'use strict';

    if (url) {
        url = url.toString().replace(/^(?:https?|ftp)\:\/\//i, '');
        url = url.toString().replace(/^www\./i, '');
        url = url.toString().replace(/\/.*/, '');
        return url;
    }
}

function url2Path(url) {

    if (url) {
        url = url.toString().replace(/^(?:https?|ftp)\:\/\//i, '');
        url = url.toString().replace(/^www\./i, '');
        url = url.toString().split("?")[0];
        url = url.toString().replace(/\/$/, '');
        url = url.toString().replace(/^\//, '');
        return url;
    }
}

function cleanHref(href) {
    if (href) {
        href = href.toString().replace(/^(?:https?|ftp)\:\/\//i, '');
        href = href.toString().replace(/\/$/, '');
        return href;
    }
}

function removeDuplicates(arr) {
    return Array.from(new Set(arr))
}