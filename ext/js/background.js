/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */


if (typeof chrome === 'undefined' && typeof browser !== 'undefined') {
    chrome = browser;
}

const BASE_URL = 'https://etrk39g2sb.execute-api.eu-west-1.amazonaws.com/production';

var
    siteList = [],
    shorts = [
        'bit.do',
        'bit.ly',
        'cutt.us',
        'goo.gl',
        'ht.ly',
        'is.gd',
        'ow.ly',
        'po.st',
        'tinyurl.com',
        'tr.im',
        'trib.al',
        'u.to',
        'v.gd',
        'x.co',
        't.co'
    ],
    toExpand = [],
    expanded = [];


function xhReq(url, METHOD = 'GET', data = null) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType('application/json');
        xhr.timeout = 10000;
        xhr.open(METHOD, url, true);

        xhr.onload = function() {
            if (xhr.status === 200) {
                //console.log("The response", xhr.response);
                resolve(xhr.responseText);
            } else {
                console.error("Rejecting request", url)
                reject(xhr.responseText);
            }
        };

        xhr.send(data);

    });
}



const sendUrlInfoRequest = async(urlsToFetch) => {
    const endpoint = BASE_URL + '/sites/';
    const data = JSON.stringify({ "urlArr": urlsToFetch })
    const response = await xhReq(endpoint, 'POST', data);
    return response;
}

const getDomainInfo = async(urls) => {

    const urlsToFetch = urls.filter(url => !cache.contains(url2Domain(url)));
    //urls.map(url => traverse.cacheSiteInfo(cache, url, {}));

    if (urlsToFetch.length > 0) {
        console.log("Fetching " + urlsToFetch.length + " domains from remote server");
        const jsonResponse = JSON.parse(await sendUrlInfoRequest(urlsToFetch));
        console.log("Got remote server response", jsonResponse)
        jsonResponse.map(responseItem => cache.insert(url2Domain(responseItem.siteId), responseItem));
    }

    return urls.map(url => traverse.getCachedSiteInfo(cache.cacheObj, url)).filter(urlInfo => urlInfo.type);
}

async function unshortenMeAPI(urlToExpand) {
    var requestUrl = 'https://unshorten.me/json/' + urlToExpand;
    return xhReq(requestUrl);
}

async function expandUrlAPI(urlToExpand) {
    const requestUrl = 'http://expandurl.com/api/v1/?url=' + urlToExpand + '&format=json&detailed=true'
    const response = await xhReq(requestUrl)
    return {
        "requested_url": urlToExpand,
        "resolved_url": JSON.parse(response).rel_meta_refresh[0].url
    }
}

async function expandLink(url) {
    const encodedUri = encodeURIComponent(url)

    let response = await unshortenMeAPI(encodedUri);
    if (!JSON.parse(response).success) {
        response = await expandUrlAPI(encodedUri)
    }
    return response;
}


const handleOnMessage = async(request) => {

    console.log(request.operation);

    switch (request.operation) {
        case 'passData':
            return ({
                sites: siteList,
                shorteners: shorts
            });

        case 'expandLink':
            response = await expandLink(request.shortLinks);
            return ({ expandedLinks: response })

        case 'getUrlInfo':
            response = await getDomainInfo(removeDuplicates(request.urls))
            console.log("Sending response from url info", response);
            return response;
        case 'togglePopup':

            await chrome.tabs.getSelected(null, function(tabs) {
                console.log("Tabs", tabs)
                chrome.tabs.sendMessage(tabs.id, request);
            });
            break;
        case 'rateLabels':
            console.log("request: ", request.data);
            //getUserInfo();
    }
}


chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    handleOnMessage(request).then(response => sendResponse(response))
    return true;
});



// toggle display of the warning UI when the pageAction is clicked
chrome.browserAction.onClicked.addListener(function(tab) {

    'use strict';


    xhReq(chrome.extension.getURL("./html/popup.html")).then(htmlFile => {


        const response = {
            operation: 'togglePopup',
            data: htmlFile
        }


        chrome.tabs.sendMessage(tab.id, response);
    });

    return true;

});
