console.log("in popup.js")

const params = new URL(document.location).searchParams;
const currentUrl = params.get('currentUrl');
console.log(currentUrl)

let description = document.querySelector('.current-url');
description.textContent = currentUrl;




console.log(mdc);
let circ = document.querySelector('.mdc-circular-progress')
console.log(circ)
const circularProgress = new mdc.circularProgress.MDCCircularProgress(circ);
circularProgress.determinate = false;
circularProgress.close();

// Submit button
const submitButton = document.querySelector("#submit-button");
mdc.ripple.MDCRipple.attachTo(submitButton);

submitButton.addEventListener('click', (event) => {
    console.log(event);
    circularProgress.open();

    const chipsInCategories = getChipsInCategory(["Bias"])
    sendChips(chipsInCategories);
    console.log(chipsInCategories)

    closePopup();

})

// Close button 
const closeButton = document.querySelector("#close-button");
closeButton.addEventListener('click', (event) => {
    closePopup();
})

const getChipsInCategory = (top3Chips) => {

    const rateUp = chipSetFirst.chips.map(chip => chip.root.attributes['data-tag-name'].value)
    const rateDown = chipSetSecond.chips.filter((chip) => {
        return top3Chips.indexOf(chip.root.attributes['data-tag-name'].value) >= 0;
    }).map(chip => chip.root.attributes['data-tag-name'].value);

    return {
        'RateUp': rateUp,
        'RateDown': rateDown
    };

}

const sendChips = (chips) => {
    const message = {
        'operation': 'rateLabels',
        'data': chips
    };
    sendPopupMessageToBackground(message);
}

const closePopup = () => {

    const message = {
        'operation': 'togglePopup',
    }
    sendPopupMessageToBackground(message);

}


//mdc.autoInit();

const labels = ['Clickbait', 'Rumor', 'Paywall', 'Fake', 'Conspiracy'];

const chipSetElFirst = document.querySelector('#chip-set-1');
const chipSetFirst = new mdc.chips.MDCChipSet(chipSetElFirst);

const chipSetElSecond = document.querySelector('#chip-set-2');
const chipSetSecond = new mdc.chips.MDCChipSet(chipSetElSecond);
initChipSet(chipSetSecond, chipSetElSecond, labels, '#icon_arrow_up');


function initChipSet(chipSet, chipSetEl, labels, icon) {
    labels.forEach(label => {
        var trailingIcon = document.querySelector(icon).cloneNode(true);
        var newChipEl = createChipEl(label, undefined, trailingIcon);
        chipSetEl.appendChild(newChipEl);
        chipSet.addChip(newChipEl);
    });
}

function createChipEl(text, leadingIcon, trailingIcon) {
    const chipTextEl = document.createElement('div');

    chipTextEl.classList.add('mdc-chip__text');
    chipTextEl.appendChild(document.createTextNode(text));

    const chipEl = document.createElement('div');
    chipEl.setAttribute("data-tag-name", text);
    chipEl.classList.add('mdc-chip');

    if (leadingIcon) {
        chipEl.appendChild(leadingIcon);
    }
    chipEl.appendChild(chipTextEl);
    if (trailingIcon) {
        chipEl.appendChild(trailingIcon);
    }
    return chipEl;
}

function addChipToSet(label, chipSet, chipSetEl, icon) {
    var trailingIcon = document.querySelector(icon).cloneNode(true);
    var newChipEl = createChipEl(label, undefined, trailingIcon);
    chipSetEl.appendChild(newChipEl);
    chipSet.addChip(newChipEl);
}

chipSetFirst.listen('MDCChip:removal', function(event) {
    let label = event.target.attributes['data-tag-name'].value;
    addChipToSet(label, chipSetSecond, chipSetElSecond, '#icon_arrow_up');
});

chipSetSecond.listen('MDCChip:removal', function(event) {
    let label = event.target.attributes['data-tag-name'].value;
    addChipToSet(label, chipSetFirst, chipSetElFirst, '#icon_cancel');
});



const sendPopupMessageToBackground = async(message) => {
    console.log("sending message in popup");
    return new Promise((resolve) => {
        return chrome.runtime.sendMessage(null, message, null, function(response) {
            resolve(response);
        });
    })
}

const message = { 'operation': 'getUrlInfo', urls: [currentUrl] }

sendPopupMessageToBackground(message).then(response => {
    console.log("response in popup: ", response);
    initChipSet(chipSetFirst, chipSetElFirst, [response[0].type], '#icon_cancel');
});
