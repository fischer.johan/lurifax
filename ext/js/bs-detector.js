/*!
 * B.S. Detector v0.2.7 (http://bsdetector.tech)
 * Copyright 2016 The B.S. Detector Authors (https://github.com/selfagency/bs-detector/graphs/contributors)
 * Licensed under LGPL-3.0 (https://github.com/selfagency/bs-detector/blob/master/LICENSE)
 */

/*global chrome,browser,self,top,console,$,JSON,MutationObserver*/
/*jslint browser: true */


// If we don't have a browser object, check for chrome.
if (typeof chrome === 'undefined' && typeof browser !== 'undefined') {
    chrome = browser;
}

const fakeRequest = async(millis) => {
    return new Promise((resolve, reject) => setTimeout(() => resolve(), millis));
}

const sendMessageToBackground = async(message) => {

    return new Promise((resolve) => {

        return chrome.runtime.sendMessage(null, message, null, function(response) {
            resolve(response);
        });

    })
}


/**
 * @description Class constructor with variable initialisation
 *
 * @method BSDetector
 */
function BSDetector() {

    'use strict';

    this.bsId = null;
    this.currentSite = null;
    this.currentUrl = '';
    this.data = [];
    this.dataType = '';
    this.debugActive = false;
    this.expandLinks = null;
    this.expanded = {};
    this.flagState = 0; // 0 initial, 1 open, -1 hidden
    this.firstLoad = true;
    this.shorts = [];
    this.shortUrls = [];
    this.siteId = '';
    this.warnMessage = '';
    this.mutationObserver = {};
    this.windowUrl = window.location.hostname;
    this.windowHref = cleanHref(window.location.href);
    this.observerRoot = null;
    this.observerFilter = null;
    this.ownHostRegExp = new RegExp(url2Path(window.location.href));
    this.lfbRegExp = new RegExp(/^https?:\/\/l\.facebook\.com\/l\.php\?u=([^&]+)/);
    this.urlRegExp = new RegExp(/^(http|https):\/\/[^ "]+$/);

}




BSDetector.prototype = {

    constructor: BSDetector,


    /**
     * @description Log debug messages, if the debug flag is set
     *
     * @method debug
     * @param {string}
     */
    debug: function() {

        'use strict';

        if (this.debugActive === true) {
            console.debug.apply(null, ['[B.S. 💩 Detector] '].concat(arguments));
        }
    },


    /**
     * @description Asynchronous loading function
     *
     * @method asynch
     * @param {string} thisFunc
     * @param {function} callback
     */
    asynch: function(thisFunc, callback) {

        'use strict';

        setTimeout(function() {
            thisFunc();
            if (typeof callback === 'function') {
                callback();
            }
        }, 10);
    },



    /**
     * @description Check if a string is valid JSON
     *
     * @method isJson
     * @param {string} string
     * @param {boolean}
     */
    isJson: function(string) {

        'use strict';

        try {
            JSON.parse(string);
        } catch (e) {
            console.error('Given string is no valid JSON');
            return false;
        }
        return true;
    },



    /**
     * @description Strip urls down to hostname
     *
     * @method cleanUrl
     * @param {string} url
     * @return {string}
     */
    cleanUrl: function(url) {

        'use strict';

        var
            testLink = '',
            thisUrl = '';

        if (this.siteId === 'facebook') {
            testLink = decodeURIComponent(url).substring(0, 30);

            if (testLink === 'https://l.facebook.com/l.php?u=' || testLink === 'http://l.facebook.com/l.php?u=') {
                thisUrl = decodeURIComponent(url).substring(30).split('&h=', 1);
                url = thisUrl;
            }

        }

        return url2Domain(url);
    },


    clearLabels: function() {

        $('a[link-checked]').each(function() {
            console.log("Clearing labels");
            $(this).removeAttr('link-checked bs-flag abs-href data-is-bs data-expanded-url data-bs-type');
        })
        console.log("number of popups before: ", $('div.bs-alert-popup').length);
        $('div.bs-alert-popup').remove();
        console.log("number of popups after: ", $('div.bs-alert-popup').length);

    },


    /**
     * @description Identify current site
     *
     * @method identifySite
     */
    identifySite: async function() {

        'use strict';
        console.log("identifySite");

        // currentSite looks for the currentUrl (window.location.hostname) in the JSON data file
        this.currentUrl = this.cleanUrl(this.windowUrl);
        console.log("location href", window.location.href);

        //this.ownHostRegExp = new RegExp(url2Path(window.location.href));



        if (self === top) {
            switch (this.currentUrl) {
                case 'www.facebook.com':
                case 'facebook.com':
                    this.siteId = 'facebook';
                    break;
                case 'twitter.com':
                    this.siteId = 'twitter';
                    break;
                default:
                    this.siteId = 'none';

                    const path = url2Path(this.windowHref);

                    const message = {
                        'operation': 'getUrlInfo',
                        urls: [path, bsd.currentUrl]
                    }

                    const response = await sendMessageToBackground(message);
                    response.map(url => {
                        console.log("URL",url);
                        bsd.siteId = 'badlink';
                        bsd.dataType = url.type;
                    })

                    break;
            }
        }

        this.debug('this.currentUrl: ', this.currentUrl);
        this.debug('this.currentSite: ', this.currentSite);
        this.debug('this.siteId: ', this.siteId);
        this.debug('this.dataType: ', this.dataType);

    },






    /**
     * @description Generate warning message for a given url
     *
     * @method warningMsg
     */
    warningMsg: function() {

        'use strict';

        var classType = '';

        switch (this.dataType) {
            case 'bias':
                classType = 'Extreme Bias';
                break;
            case 'conspiracy':
                classType = 'Conspiracy Theory';
                break;
            case 'fake':
                classType = 'Fake News';
                break;
            case 'junksci':
                classType = 'Junk Science';
                break;
            case 'rumors':
                classType = 'Rumor Mill';
                break;
            case 'satire':
                classType = 'Satire';
                break;
            case 'state':
                classType = 'State News Source';
                break;
            case 'hate':
                classType = 'Hate Group';
                break;
            case 'clickbait':
                classType = 'Clickbait';
                break;
            case 'caution':
                classType = 'Caution';
                break;
            case 'test':
                classType = 'Test';
                break;
            default:
                classType = 'Classification Pending';
                break;
        }


        if (this.dataType === 'caution') {
            this.warnMessage = 'Caution: Source may be reliable but contents require further verification.';
        } else {
            this.warnMessage = 'Warning: This may not be a reliable source. (' + classType + ')';
        } -

        this.debug('this.warnMessage: ', this.warnMessage);
    },



    /**
     * @description Flag entire site
     *
     * @method flagSite
     */
    flagSite: function() {

        'use strict';

        var navs = $('nav, #nav, #navigation, #navmenu');

        if (this.flagState !== 0) {
            return;
        }

        this.flagState = 1;
        this.warningMsg();

        if ($(navs)) {
            $(navs).first().addClass('bs-alert-shift');
        } else {
            $('body').addClass('bs-alert-shift');
        }

        if (this.dataType === 'caution') {
            $('body').prepend('<div class="bs-alert bs-caution"></div>');
        } else {
            $('body').prepend('<div class="bs-alert"></div>');
        }

        $('.bs-alert').append('<div class="bs-alert-close">✕</div>');
        $('.bs-alert').append('<span>' + this.warnMessage + '</span>');

        $('.bs-alert-close').on('click', function() {
            $(navs).first().removeClass('bs-alert-shift');
            $('body').removeClass('bs-alert-shift');
            $('.bs-alert').remove();
        });
    },



    /**
     * @description Make flags visible
     *
     * @method showFlag
     */
    showFlag: function() {

        'use strict';

        this.flagState = 1;
        $('.bs-alert').show();
    },



    /**
     * @description Make flags invisible
     *
     * @method hideFlag
     */
    hideFlag: function() {

        'use strict';
        if (this.flagState === 1) {
            this.flagState = -1;
            $('.bs-alert').hide();
        }
    },



    /**
     * @description Get the hostname of a given element's link
     *
     * @method getHost
     * @param {object} $element
     * @return {string}
     */
    getHost: function($element) {

        'use strict';

        var thisUrl = '';
        if ($element.attr('data-expanded-url') !== null && $element.attr('data-expanded-url') !== undefined) {
            thisUrl = $element.attr('data-expanded-url');
        } else {
            thisUrl = $element.attr('href');
        }
        if (thisUrl !== null && thisUrl !== undefined) {
            thisUrl = this.cleanUrl(thisUrl);
        }

        return thisUrl;
    },


    expandLink: async function(link) {

        console.log("Expanding Links");

        const message = {
            'operation': 'expandLink',
            'shortLinks': link.toString()
        }

        const response = await sendMessageToBackground(message)

        if (bsd.isJson(response.expandedLinks.toString())) {
            this.expanded = JSON.parse(response.expandedLinks);
            $('a[href="' + this.expanded.requested_url + '"]').attr('data-expanded-url', this.expanded.resolved_url);
        } else {
            bsd.debug('Could not expand shortened link');
            bsd.debug('Response: ' + response);
        }
        bsd.setBSLabel();


    },


    /**
     * @description Target links
     *
     * @method targetLinks
     */
    targetLinks: function() {


        'use strict';


        // find and label external links
        $('a[href]:not([href^="#"]), a[data-expanded-url] ').not('[link-checked]').each(function() {

            var
                testLink = '',
                thisUrl = '',
                matches = null;

            // exclude links that have the same hostname
            let currHref = this.href;
            //console.log("window href: ", bsd.ownHostRegExp);
            //console.log("url: ", currHref);


            testLink = decodeURIComponent(currHref);

            if (bsd.shorts.indexOf(url2Domain(currHref)) > -1) {
                bsd.expandLink(testLink);
            }


            // convert facebook urls
            if (bsd.siteId === 'facebook') {

                testLink = decodeURIComponent(this.href);
                if (matches = bsd.lfbRegExp.exec(this.href)) {
                    thisUrl = decodeURIComponent(matches[1]);
                }
                if (thisUrl !== '') {
                    //$(this).attr('data-external', true);
                    $(this).attr('data-expanded-url', thisUrl);
                }
            }
        });
        this.setBSLabel();

    },




    filterHref: function(index, element) {
        $(element).attr('link-checked', true);

        if(!bsd.urlRegExp.test(element.href)){
            return false;
        }

        if (bsd.siteId === 'badlink') {
            

            if( url2Domain(window.location.href) === url2Domain(element.href)){        
                return bsd.flagState !== 1;
            }
            
            return true;
        }
        return bsd.urlRegExp.test(element.href);
    },

    setBSLabel: async function() {
        // process external links

        let urlsToCheck = $('a:not([link-checked]), a[data-expanded-url]:not([data-is-bs])').filter(this.filterHref)
            .map((index, value) => {
                $(value).attr('abs-href', value.href);
                return url2Path($(value).attr('data-expanded-url')) || url2Path(value.href);
            }).get();

        console.log("Length after", urlsToCheck.length)

        const message = {
            'operation': 'getUrlInfo',
            'urls': urlsToCheck
        }

        const response = await sendMessageToBackground(message);

        console.log("Received response from backend", response);

        response.map(urlResponse => {

            const selector = `a[abs-href*='${urlResponse.siteId}'],[data-expanded-url*='${urlResponse.siteId}']`

            $.map($(selector), (value) => {
                $(value).attr('data-is-bs', true);
                $(value).attr('data-bs-type', urlResponse.type);
            });


        })
    },

    //Checks if warning message already exists
    warningExists: function($badlinkWrapper) {
        let wrapperSiblings = $badlinkWrapper.siblings('div.bs-alert-popup');
        return wrapperSiblings.length !== 0;
    },


    /**
     * @description Flag links
     *
     * @method flagPost
     * @param {object} $badlinkWrapper
     */
    flagPost: function($badlinkWrapper) {

        if (!$badlinkWrapper.hasClass('bs-flag') && !this.warningExists($badlinkWrapper)) {

            if (this.dataType === 'caution') {
                $badlinkWrapper.before('<div class="bs-alert-inline warning">' + this.warnMessage + '</div>');
            } else {
                $badlinkWrapper.before('<div class="bs-alert-inline">' + this.warnMessage + '</div>');
            }

            $badlinkWrapper.addClass('bs-flag');
        }
    },

    //<div class="bs-alert-popup-short-message">⚠️</div>

    flagLink: function($link) {
        if (!$link.hasClass('bs-flag') && !this.warningExists($link)) {
            $link.before(`
            

            
            <div class="bs-alert-popup shadow">
                <div class="bs-alert-popup-short-message">⚠️</div>
                <div class="bs-alert-popup-message">${this.warnMessage}</div>
            </div>`);

            $link.addClass('bs-flag');
        }
    },


    /**
     * @description
     *
     * @method setAlertOnPosts
     * @param {string}
     */

    setAlertOnPosts: async function() {

        'use strict';

        bsd.targetLinks();

        $('a[data-is-bs="true"]').each(function() {
            bsd.dataType = $(this).attr('data-bs-type');
            bsd.warningMsg();

            bsd.debug('Current warning link: ', this);
            bsd.debug('bsd.dataType: ', bsd.dataType);


            switch (bsd.siteId) {
                case 'facebook':
                    if ($(this).parents('._1dwg').length > 0) {
                        bsd.flagLink($(this).closest('.mtm'));
                    }
                    if ($(this).parents('.UFICommentContent').length > 0) {
                        bsd.flagLink($(this).closest('.UFICommentBody'));
                    }
                    break;
                case 'twitter':
                    if ($(this).parents('article').length > 0) {
                        bsd.flagLink($(this).closest('article'));
                    }
                    break;
                case 'badlink':
                    bsd.flagLink($(this));
                    break;
                case 'none':
                    bsd.flagLink($(this));
                    break;
                default:
                    break;
            }
        });

        this.firstLoad = false;
    },

    /**
     * @description Main run this after a mutation
     *
     * @method observerCallback
     */
    observerCallback: function() {

        'use strict';

        bsd.debug('observerCallback');
        bsd.observerRoot.mutationSummary("disconnect");
        bsd.observerExec();
    },

    /**
     * @description Scan for posts, turn on the observer, and scan again for more changes
     *
     * @method observerExec
     */
    observerExec: async function() {

        'use strict';

        bsd.debug('observerExec');



        console.log("bsd href: ", bsd.windowHref);
        console.log("window href: ", cleanHref(window.location.href))
        console.log("window url: ", bsd.windowUrl);
        console.log("siteId: ", bsd.siteId);

        if (bsd.windowHref !== cleanHref(window.location.href)) {
            this.clearLabels();
        }
        // update href
        bsd.windowHref = cleanHref(window.location.href);

        await bsd.identifySite();
        if (this.siteId === 'badlink') {
            if (bsd.flagState === -1) {
                bsd.showFlag();
            }
            if (bsd.flagState === 0) {
                this.flagSite();
            }
        } else {
            this.hideFlag();
        }
        this.setAlertOnPosts();

        window.setTimeout(this.observe, 500);
        window.setTimeout(this.setAlertOnPosts, 1000);
    },

    /**
     * @description Turn on the mutation observer
     *
     * @method observe
     */
    observe: function() {

        'use strict';

        bsd.debug('observe', bsd.observerCallback, bsd.observerFilter, bsd.observerRoot);
        bsd.observerRoot.mutationSummary("connect", bsd.observerCallback, bsd.observerFilter);
    },

    togglePopup: function(data) {
        console.log("toggel popup")

        // Must be declared at web_accessible_resources in manifest.json

        if ($('#WITOpopup').length > 0) {
            $('#WITOpopup').remove();
        } else {
            var iframe = document.createElement('iframe');
            let src = chrome.runtime.getURL('html/popup.html') + `?currentUrl=${url2Path(window.location.href)}`;
            iframe.src = src;
            iframe.id = "WITOpopup"
            $('body').prepend(iframe);
        }


    },

    /**
     * @description Main execution script
     *
     * @method execute
     */
    execute: async function() {


        'use strict';
        if (this.firstLoad === true) {
            await this.identifySite();

            if (this.siteId === 'badlink') {
                this.flagSite();
            }

            this.firstLoad = false;
        }

        switch (this.siteId) {
            case 'facebook':
                this.observerRoot = $("body");
                this.observerFilter = [{ element: "div" }];
                break;
            case 'twitter':
                this.observerRoot = $("body");
                this.observerFilter = [{ element: "div" }];
                break;
            case 'badSite':
                break;
            case 'none':
            default:
                this.observerRoot = $("body");
                this.observerFilter = [{ element: "div" }];
                break;
        }

        this.observerExec();

    }
};


/**
 * @description Grab data from background and execute extension
 * @link https://developer.chrome.com/extensions/runtime#method-sendMessage
 *
 * @method chrome.runtime.sendMessage
 * @param {string} extensionId
 * @param {mixed} message
 * @param {object} options
 * @param {function} responseCallback
 */
if (window === window.top || url2Domain(window.location.hostname) == 'twitter.com') {
    var bsd = new BSDetector();


    /**
     * @description Grab data from background and execute extension
     *
     * @method
     * @param {string}
     */
    sendMessageToBackground({ 'operation': 'passData' }).then(response => {

        //console.log('Received data from background', response.sites);
        //bsd.data = response.sites;
        bsd.shorts = response.shorteners;

        $(document).ready(function() {

            bsd.execute();
        });
    });
}




/**
 * @description Listen for messages but only in the top frame
 * @link https://developer.chrome.com/extensions/runtime#event-onMessage
 *
 * @method chrome.runtime.onMessage.addListener
 * @param {function}
 */
if (window.top === window) {


    chrome.runtime.onMessage.addListener(function(message) {

        'use strict';

        switch (message.operation) {
            case 'flagSite':
                bsd.dataType = message.type;
                bsd.flagSite();
                break;
            case 'toggleFlag':
                if (bsd.flagState === 1) {
                    bsd.hideFlag();
                } else if (bsd.flagState === -1) {
                    bsd.showFlag();
                }
                break;
            case 'togglePopup':
                //$('body').prepend(message.url);
                //$('body').prepend(message.data);

                bsd.togglePopup(message.data);
        }
    });
}
