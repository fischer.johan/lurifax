# WITO - What is true online




A browser extension for both Chrome and Mozilla-based browsers, **WITO** searches all links on a given webpage for references to unreliable sources, checking against a manually compiled list of domains. It then provides visual warnings about the presence of questionable links or the browsing of questionable websites:


Example domain classifications (in flux) include:

-   **Fake News:** Sources that fabricate stories out of whole cloth with the intent of pranking the public.
-   **Satire:** Sources that provide humorous commentary on current events in the form of fake news.
-   **Extreme Bias:** Sources that traffic in political propaganda and gross distortions of fact.
-   **Conspiracy Theory:** Sources that are well-known promoters of kooky conspiracy theories.
-   **Rumor Mill:** Sources that traffic in rumors, innuendo, and unverified claims.
-   **State News:** Sources in repressive states operating under government sanction.
-   **Junk Science:** Sources that promote pseudoscience, metaphysics, naturalistic fallacies, and other scientifically dubious claims.
-   **Hate Group:** Sources that actively promote racism, misogyny, homophobia, and other forms of discrimination.
-   **Clickbait:** Sources that are aimed at generating online advertising revenue and rely on sensationalist headlines or eye-catching pictures.
-   **Proceed With Caution:** Sources that may be reliable but whose contents require further verification.

---

## Setup

Add WITO plugin to your chrome browser:

 * Navigate to <chrome://extensions>
 * Press "Load unpacked"
 * Open the ext folder in WITO root

WITO plugin should now be added






---
